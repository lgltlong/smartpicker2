<?php
if (!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
    exit('Access Denied');
}
require './source/plugin/csdn123com_kuaibao/common.fun.php';
if ($_GET['formhash'] == FORMHASH && empty($_GET['catch']) == false && $_GET['catch'] == 'yes') {
    
	$fromurl = preg_replace('/\?.+/','',$fromurl);
	$fromurl = daddslashes($_GET['fromurl']);
    $chk = DB::fetch_first("SELECT ID,tid FROM " . DB::table('csdn123kuaibao_news') . " WHERE fromurl='" . $fromurl . "' LIMIT 1");
    if (count($chk) > 0 && $chk['tid'] > 0) {
        $threadUrl = $_G['siteurl'] . "forum.php?mod=viewthread&tid=" . $chk['tid'];
        echo '<div style="line-height:30px;font-size:22px;">' . lang('plugin/csdn123com_kuaibao', 'catch_ok') . '<br><a href="' . $threadUrl . '" target="_blank">' . $threadUrl . '</a></div>';
        exit;
    }
    $uidstr = daddslashes($_GET['uidstr']);
    if (preg_match('/[a-z]/i', $uidstr) == 1) {
        cpmsg('csdn123com_kuaibao:uid_err', '', 'error');
        exit;
    }
    if (strpos($fromurl, 'kuaibao.qq.com') === false) {
        cpmsg('csdn123com_kuaibao:url_err', '', 'error');
        exit;
    }
    if (count($chk) == 0) {
        $threadValue['uidstr'] =  $uidstr;
        $threadValue['fid'] = intval($_GET['fid']);
        $threadValue['typeid'] = intval($_GET['threadtypeid']);
        $threadValue['fromurl'] =  $fromurl;
        $ID = DB::insert('csdn123kuaibao_news', $threadValue, true);
    } else {
        $ID = $chk["ID"];
    }
    $recode = send_thread($ID);
    if ($recode == 'ok') {
        $success_Url = 'action=plugins&operation=config&do=' . $pluginid . '&identifier=csdn123com_kuaibao&pmod=success';
        cpmsg('csdn123com_kuaibao:catch_ok', $success_Url, 'succeed');
    } else {
        cpmsg('csdn123com_kuaibao:catch_err', '', 'error');
    }
} else {
    require_once libfile('function/forumlist');
    include template("csdn123com_kuaibao:catch");
}
