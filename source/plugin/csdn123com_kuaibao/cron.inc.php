<?php
if (!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {

	exit('Access Denied');

}
require './source/plugin/csdn123com_kuaibao/common.fun.php';
$server_url='action=plugins&operation=config&do=' . $pluginid . '&identifier=csdn123com_kuaibao&pmod=cron';
if ($_GET['formhash'] == FORMHASH && empty($_GET['cron']) == false && $_GET['cron'] == 'yes') {

	$keyword = daddslashes($_GET['keyword']);
	$uidstr = daddslashes($_GET['uidstr']);
	if (preg_match('/[a-z]/i', $uidstr) == 1) {

		cpmsg('csdn123com_kuaibao:uid_err', '', 'error');
		exit;
	}
	if (strlen($keyword)<2) {

		cpmsg('csdn123com_kuaibao:keyword_empty', '', 'error');
		exit;

	}
	$cronArr=array();
	$cronArr['keyword']=$keyword;
	$cronArr['uidstr']=$uidstr;
	$cronArr['fid']=daddslashes($_GET['fid']);
	$cronArr['typeid']=daddslashes($_GET['threadtypeid']);
	DB::insert('csdn123kuaibao_cron',$cronArr);
	cpmsg('csdn123com_kuaibao:cron_ok',$server_url, 'succeed');
	
	

} elseif($_GET['formhash'] == FORMHASH && empty($_GET['cron_add']) == false && $_GET['cron_add'] == 'yes') {

	require_once libfile('function/forumlist');
	include template("csdn123com_kuaibao:cron_add");	

} elseif($_GET['formhash'] == FORMHASH && empty($_GET['modify']) == false && is_numeric($_GET['modify'])==true) {

	$modify_id = intval($_GET['modify']);
    $postRs = DB::fetch_first("SELECT * FROM " . DB::table("csdn123kuaibao_cron") . " WHERE ID=" . $modify_id);
    $typeclassArr = C::t('forum_threadclass')->fetch_all_by_fid($postRs['fid']);
	require_once libfile('function/forumlist');
	include template("csdn123com_kuaibao:cron_modify");

} elseif($_GET['formhash'] == FORMHASH && empty($_GET['modifysmt']) == false && $_GET['modifysmt']=='yes') {
	
	$cronArr = array();
    $cronArr['keyword'] = daddslashes($_GET['keyword']);
    $cronArr['fid'] = daddslashes($_GET['fid']);
    $cronArr['typeid'] = daddslashes($_GET['threadtypeid']);
    $cronArr['uidstr'] = daddslashes($_GET['uidstr']);
    DB::update('csdn123kuaibao_cron', $cronArr, 'ID=' . intval($_GET['modify_update_id']));
    cpmsg('csdn123com_kuaibao:modify_success', $server_url, 'succeed');

} elseif($_GET['formhash'] == FORMHASH && empty($_GET['del']) == false && is_numeric($_GET['del'])==true) {

	DB::delete('csdn123kuaibao_cron','ID=' . intval($_GET['del']));	
	cpmsg('csdn123com_kuaibao:cron_del_ok',$server_url, 'succeed');

} else {
	
	$postRs = DB::fetch_all("SELECT * FROM " . DB::table("csdn123kuaibao_cron") . " ORDER BY  catchnum DESC");
	include template("csdn123com_kuaibao:cron_list");	
	
	
}

