<?php
if (!defined('IN_DISCUZ')) {
    exit('Access Denied');
}
function str_replace_once($needle, $replace, $haystack) {
    $pos = strpos($haystack, $needle);
    if ($pos === false) {
        return $haystack;
    }
    return substr_replace($haystack, $replace, $pos, strlen($needle));
}
function ext_getuserbyuid($uid)
{
	$userInfo=getuserbyuid($uid,1);
	if(is_array($userInfo)==false || is_numeric($userInfo['uid'])==false || $userInfo['uid']<=0)
	{
		$userInfo=DB::fetch_first("SELECT uid,username FROM " . DB::table('common_member') . " WHERE uid=" . intval($uid) . "  LIMIT 1");		
	}
	return array('uid'=>$userInfo['uid'],'username'=>$userInfo['username']);	
}
function convert_img($tid, $pid, $uid, $message,$allowhtml=0) {
    global $_G;
	if($allowhtml==0)
	{
		preg_match_all("/(\[img\]|\[img=\d{1,4}[x|\,]\d{1,4}\])\s*([^\[\<\r\n]+?)\s*\[\/img\]/is", $message, $imglist, PREG_SET_ORDER);
	} else {
		preg_match_all('/<img[^<>]*src\s*=\s*([\'"]?)([^\'">]*)\1[^<>]*>/i', $message, $imglist, PREG_SET_ORDER);
	}
	$nopic=$_G['siteurl'] . 'source/plugin/csdn123com_kuaibao/template/nopic.JPG';
    if (count($imglist) > 0) {
        foreach ($imglist as $img) {
		
			$message=str_replace($img[2],$nopic,$message);
			
		}
        return $message;
    } else {
        return $message;
    }
}
function send_thread($ID = NULL,$fromurl = NULL) {
	
    global $_G;
    $recode = array();
    if (is_null($fromurl) && is_null($ID)) {
        return 'no1';
    }
    if (is_numeric($ID)) {
        $hzw_news = DB::fetch_first("SELECT * FROM " . DB::table('csdn123kuaibao_news') . " WHERE ID=" . $ID);
		$fromurl=$hzw_news['fromurl'];
    } else {
		$fromurl = preg_replace('/\?.+/','',$fromurl);
		$fromurl = daddslashes($fromurl);
        $hzw_news = DB::fetch_first("SELECT * FROM " . DB::table('csdn123kuaibao_news') . " WHERE fromurl='" . $fromurl . "'  LIMIT 1");
		$ID=$hzw_news['ID'];
    }
    if (count($hzw_news) > 0 && $hzw_news['tid'] > 0) {
        return 'ok';
    }
    $uidstr = $hzw_news['uidstr'];
    $uidArr = explode(',', $uidstr);
    $uidCount = count($uidArr);
    $remoteUrl = array();
    $remoteUrl['SN'] = '2017050517GYbc2G0Bfw';
    $remoteUrl['RevisionID'] = '72903';
    $remoteUrl['RevisionDateline'] = '1492570804';
    $remoteUrl['SiteUrl'] = 'http://localhost/dzutf8/';
    $remoteUrl['ClientUrl'] = 'http://localhost/dzutf8/';
    $remoteUrl['SiteID'] = '9FABBE0A-B684-2883-D9F6-A67EBF7AA40A';
    $remoteUrl['QQID'] = 'ED90D1D3-B815-BB4B-3940-2BE3EEBB057D';
    $remoteUrl['safecode'] = 'bcabd89a20cecea1da23842932ba22ed';
    $remoteUrl['SiteUrl2'] = $_G['siteurl'];
    $remoteUrl['ip'] = $_SERVER['REMOTE_ADDR'];
    $remoteUrl['url'] = $fromurl;
	$remoteUrl['siteur1'] = $_SERVER['HTTP_HOST'];
    $fetchUrl = "http://discuz.csdn123.net/catch/kuaibao/catch.php";
    $htmlcode = dfsockopen($fetchUrl, 0, $remoteUrl);	
	if($htmlcode=='no2')
	{
		$htmlcode = dfsockopen($fromurl);
		if(strlen($htmlcode)<100)
		{
			$htmlcode = dfsockopen($fromurl,0,'','',FALSE,'',15,TRUE,'URLENCODE',FALSE);
		}
		$htmlcode = base64_encode($htmlcode);
		$remoteUrl['htmlcode']=$htmlcode;		
		$htmlcode = dfsockopen($fetchUrl,0,$remoteUrl);
	}	
	if (strlen($htmlcode) < 100) {
        return 'no3';
	}
	$htmlcode = preg_replace('/^\s+|\s+$/','',$htmlcode);
	$htmlcode = dunserialize($htmlcode);
	if(is_array($htmlcode)===false)
	{
        return 'no4';
	}
	$_G['forum']['fid'] = $hzw_news['fid'];
	$forumInfo=C::t('forum_forum')->fetch_info_by_fid($hzw_news['fid']);
	$threadtypeid = $hzw_news['typeid'];	
	$post_text = $htmlcode['firstPost'];
	$post_text = diconv($post_text,'UTF-8');
	$post_text = str_ireplace('https://','http://',$post_text);
	$post_text = $post_text . '<div style="color:red"><br><br><br><br><br>' . lang('plugin/csdn123com_kuaibao', 'try_info') . '</div>';
	$post_text = $post_text . '<a href="http://addon.discuz.com/?@csdn123com_kuaibao.plugin">http://addon.discuz.com/?@csdn123com_kuaibao.plugin</a>';
	require_once libfile('function/editor');
	if($forumInfo['allowhtml']!=1)
	{		
		$post_text = html2bbcode($post_text);	
		$post_text = html_entity_decode($post_text);
	}
	$firstUid = rand(1,200);
	$firstUid = $firstUid % $uidCount;
	$firstUid = $uidArr[$firstUid];
	$UserInfo = ext_getuserbyuid($firstUid);
	$PostUserInfo = $UserInfo;
	$modthread = C::m('forum_thread');
	$params = array();
	if(empty($hzw_news['subject']))
	{
		$title = diconv($htmlcode['title'],'UTF-8');
		DB::update('csdn123kuaibao_news',array('subject'=>$title),'ID=' . $hzw_news['ID']);
	} else {
		$title = $hzw_news['subject'];
	}
	$title=cutstr($title,70);
	$params['subject'] = $title;
	$params['message'] = '[tmp]';
	$params['typeid'] = 0;
	$params['sortid'] = 0;
	$params['special'] = 0;
	$params['publishdate'] = time() - 3600;
	$params['readperm'] = 0;
	$params['allownoticeauthor'] = 1;
	$params['usesig'] = 1;
	if($forumInfo['allowhtml']==1)
	{
		$params['htmlon'] = 1;
	}
	$modthread->newthread($params);	
	$tid = $modthread->tid;
	$pid = $modthread->pid;
	$threadData = array();
	$threadData['author'] = $UserInfo['username'];
	$threadData['authorid'] = $UserInfo['uid'];
	$threadData['typeid'] = $threadtypeid;
	$threadData['subject'] = $title;
	DB::update('forum_thread', $threadData, 'tid=' . $tid);
	$postData = array();
	$post_text = convert_img($tid, $pid, $UserInfo['uid'], $post_text,$forumInfo['allowhtml']);
	$postData['message'] = $post_text;
	if (strpos($post_text, '[attach]') !== false) {
		$postData['attachment'] = 2;
		require_once libfile('function/post');				
		$temp_uid = $_G['uid'];
		$temp_ismoderator = $_G['forum']['ismoderator'];
		$temp_forumpicstyle=$_G['setting']['forumpicstyle'];
		$temp_thumbwidth = $_G['setting']['forumpicstyle']['thumbwidth'];
		$temp_thumbheight = $_G['setting']['forumpicstyle']['thumbheight'];
		$_G['uid']=$UserInfo['uid'];
		$_G['forum']['ismoderator']=1;
		$_G['setting']['forumpicstyle']=array();
		$_G['setting']['forumpicstyle']['thumbwidth'] = 160;
		$_G['setting']['forumpicstyle']['thumbheight'] = 160;			
		setthreadcover($pid,$tid);
		$_G['uid'] = $temp_uid;
		$_G['forum']['ismoderator'] = $temp_ismoderator;
		$_G['setting']['forumpicstyle'] = $temp_forumpicstyle;
		$_G['setting']['forumpicstyle']['thumbwidth'] = $temp_thumbwidth;
		$_G['setting']['forumpicstyle']['thumbheight'] = $temp_thumbheight;
	}
	$postData['author'] = $UserInfo['username'];
	$postData['authorid'] = $UserInfo['uid'];
	$postData['bbcodeoff'] = 0;
	if($forumInfo['allowhtml']==1)
	{
		$postData['htmlon'] = 1;
	}
	DB::update('forum_post', $postData, 'pid=' . $pid);
	unset($postData);
	unset($params);
	$postitem_count=count($htmlcode['postitem']);
	$postitem_intval=3600/$postitem_count;
	$postitem_intval=intval($postitem_intval);
	if(is_numeric($UserInfo['uid']))
	{
		DB::query('update ' . DB::table('common_member_count') . ' set threads=threads+1 where uid=' . $UserInfo['uid']);
	}
	$lastpostArr=array();
	$lastpostArr['lastpost']=time();
	$lastpostArr['lastposter']=$PostUserInfo['username'];
	$lastpostArr['views']=rand(90,500);
	$replies = C::t('forum_post')->count_visiblepost_by_tid($tid);
	$replies = intval($replies) - 1;
	$lastpostArr['replies']=$replies;
	DB::update('forum_thread', $lastpostArr, 'tid=' . $tid);
	if ($tid > 0) {

        if (is_numeric($ID)) {
			DB::update('csdn123kuaibao_news',array('tid'=>$tid),"ID=" . $ID);
		} else {
			DB::update('csdn123kuaibao_news',array('tid'=>$tid),"fromurl='" . $fromurl . "'");
		}
		$lastUserName = $UserInfo['username'];
		$lastUserName = $tid . "\t" . daddslashes($title) . "\t" . $_G['timestamp'] . "\t" . daddslashes($lastUserName);
		DB::query("update " . DB::table('forum_forum') . " set threads=threads+1,posts=posts + " . $postitem_count . ",lastpost='" . $lastUserName . "',todayposts=todayposts + " . $postitem_count . " where fid=" . $hzw_news['fid']);
        return 'ok';		

	} else {
		
        return 'no5';

	}

}
function getRndUid() {
    $uidarray = DB::fetch_all('select uid from ' . DB::table('csdn123kuaibao_reguser') . ' order by RAND() limit 160');
    foreach ($uidarray as $uidvalue) {
        $uidstr = $uidvalue['uid'] . ',' . $uidstr;
    }
    $uidstr = substr($uidstr, 0, -1);
    if ($uidstr == "" || empty($uidstr)) {
        return 1;
    } else {
        return $uidstr;
    }
}
function getFidName($fid) {
    $fidinfo = C::t('forum_forum')->fetch_info_by_fid($fid);
    return $fidinfo['name'];
}
function getTypeidName($typeid, $fid) {
    if ($typeid == 0) {
        return '';
    }
    $typeidInfo = C::t('forum_threadclass')->fetch_all_by_typeid_fid($typeid, $fid);
    if (empty($typeidInfo) || count($typeidInfo) == 0) {
        return '';
    } else {
        return $typeidInfo[$typeid]['name'];
    }
}
function kuaiboRndNews()
{
	global $_G;
	$siteurl=$_G['siteurl'];
	$siteurl=md5($siteurl);
	$mac=substr($siteurl,1,12);
	$mac=preg_replace('/(..)(..)(..)(..)(..)(..)/','$1:$2:$3:$4:$5:$6',$mac);
	$uid=substr($siteurl,1,5);
	$kuaibaoUrl='http://r.cnews.qq.com/getSubNewsInterest';
	$kuaibaoArg=array();
	$kuaibaoArg['uid']='27c4d311330' . $uid;
	$kuaibaoArg['store']='17';
	$kuaibaoArg['hw']='HUAWEI_H60-L11';
	$kuaibaoArg['devid']='27c4d311330' . $uid;
	$kuaibaoArg['appversion']='2.9.20';
	$kuaibaoArg['screen_width']='1080';
	$kuaibaoArg['mac']=$mac;
	$kuaibaoArg['appver']='19_areading_2.9.20';
	$kuaibaoArg['android_id']='27c4d311330' . $uid;
	$kuaibaoArg['origin_imei']='000000000000000';
	$kuaibaoArg['sceneid']='';
	$kuaibaoArg['mid']='074e08f861454417401ba527066a8e0f2d9' . $uid;
	$kuaibaoArg['apptype']='android';
	$kuaibaoArg['screen_height']='1776';
	$kuaibaoArg['oldadcode']='441602';
	$kuaibaoArg['omgbizid']='a1b1ab0b7b25aa4f90ba5b22f924e772a2ce00802' . $uid;
	$kuaibaoArg['Cookie']=' lskey=; luin=; skey=; uin=; logintype=0;';
	$kuaibaoArg['qn-rid']='74bb59f5-133d-4073-a2bb-e01e4c4' . $uid;
	$kuaibaoArg['currentTab']='kuaibao';
	$kuaibaoArg['direction']='0';
	$kuaibaoArg['qn-sig']='204d84de16398fdfc2bd5cc20cf' . $uid;
	$kuaibaoArg['activefrom']='icon';
	$kuaibaoArg['logfrom']='0';
	$kuaibaoArg['chlid']='daily_timeline';
	$kuaibaoArg['qqnetwork']='wifi';
	$kuaibaoArg['imsi_history']='000000000000000';
	$kuaibaoArg['forward']='0';
	$kuaibaoArg['imsi']='000000000000000';
	$kuaibaoArg['omgid']='a86859ff81f6774c6f7a1b0ccd884e55498600102' . $uid;
	$re_kuaibaoUrl=$kuaibaoUrl . '?' . http_build_query($kuaibaoArg,'','&',PHP_QUERY_RFC3986);
	if(strlen($re_kuaibaoUrl)<400)
	{
		$re_kuaibaoUrl=$kuaibaoUrl . '?' . http_build_query($kuaibaoArg);
	}
	return $re_kuaibaoUrl;
	
}
function kuaibaoKeyword($keyword)
{
	if(empty($keyword))
	{
		return '';
	}
	global $_G;
	$siteurl=$_G['siteurl'];
	$siteurl=md5($siteurl);
	$mac=substr($siteurl,1,12);
	$mac=preg_replace('/(..)(..)(..)(..)(..)(..)/','$1:$2:$3:$4:$5:$6',$mac);
	$uid=substr($siteurl,1,5);
	$kuaibaoUrl='http://r.cnews.qq.com/search';
	$kuaibaoArg=array();
	$kuaibaoArg['uid']='27c4d311330' . $uid;
	$kuaibaoArg['store']='17';
	$kuaibaoArg['hw']='HUAWEI_H60-L11';
	$kuaibaoArg['devid']='27c4d3113307f538';
	$kuaibaoArg['appversion']='2.9.20';
	$kuaibaoArg['screen_width']='1080';
	$kuaibaoArg['mac']=$mac;
	$kuaibaoArg['appver']='19_areading_2.9.20';
	$kuaibaoArg['android_id']='27c4d311330' . $uid;
	$kuaibaoArg['origin_imei']='000000000000000';
	$kuaibaoArg['sceneid']='';
	$kuaibaoArg['mid']='074e08f861454417401ba527066a8e0f2d' . $uid;
	$kuaibaoArg['apptype']='android';
	$kuaibaoArg['screen_height']='1776';
	$kuaibaoArg['qqnetwork']='wifi';
	$kuaibaoArg['omgbizid']='a1b1ab0b7b25aa4f90ba5b22f924e772a2ce008021230b';
	$kuaibaoArg['Cookie']=' lskey=; luin=; skey=; uin=; logintype=0;';
	$kuaibaoArg['qn-rid']='96d88f55-3f5b-4371-8453-d0dfef6a0f43';
	$kuaibaoArg['currentTab']='kuaibao';
	$kuaibaoArg['imsi_history']='000000000000000';
	$kuaibaoArg['query']=$keyword;
	$kuaibaoArg['qn-sig']='c124f08b0a968902847ee7adaa39b0d7';
	$kuaibaoArg['activefrom']='icon';
	$kuaibaoArg['imsi']='000000000000000';
	$kuaibaoArg['omgid']='a86859ff81f6774c6f7a1b0ccd884e5549860010210b1b';
	$re_kuaibaoUrl=$kuaibaoUrl . '?' . http_build_query($kuaibaoArg,'','&',PHP_QUERY_RFC3986);
	if(strlen($re_kuaibaoUrl)<400)
	{
		$re_kuaibaoUrl=$kuaibaoUrl . '?' . http_build_query($kuaibaoArg);
	}
	return $re_kuaibaoUrl;

}
