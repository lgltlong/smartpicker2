<?php
if (!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
    exit('Access Denied');
}
$server_url = 'action=plugins&operation=config&do=' . $pluginid . '&identifier=csdn123com_kuaibao&pmod=admin_user';
if ($_GET['formhash'] == FORMHASH && empty($_GET['deluser']) == false && $_GET['deluser'] == 'yes') {
    $uidarray = $_GET['uidarray'];
    foreach ($uidarray as $uid) {
        DB::delete('csdn123kuaibao_reguser', 'uid=' . intval($uid));
    }
    include template('csdn123com_kuaibao:delete_user_list');
} elseif ($_GET['formhash'] == FORMHASH && empty($_GET['modifypic']) == false && is_numeric($_GET['modifypic']) == true) {
    loaducenter();
    $avatarhtml = uc_avatar($_GET['modifypic']);
    include template('csdn123com_kuaibao:uc_avatar');
} elseif ($_GET['formhash'] == FORMHASH && empty($_GET['output']) == false && $_GET['output'] == 'yes') {
    $uidarray = DB::fetch_all('select uid from ' . DB::table('csdn123kuaibao_reguser') . ' order by uid desc');
    foreach ($uidarray as $uidvalue) {
        $uidstr = $uidvalue['uid'] . ',' . $uidstr;
    }
    $uidstr = substr($uidstr, 0, -1);
    include template('csdn123com_kuaibao:output_user_list');
} else {
    $user_list = DB::fetch_all('SELECT * FROM ' . DB::table('csdn123kuaibao_reguser') . ' ORDER BY uid DESC');
    include template('csdn123com_kuaibao:user_list');
}
