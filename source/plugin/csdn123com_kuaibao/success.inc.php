<?php

if(!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {

	exit('Access Denied');
}
require './source/plugin/csdn123com_kuaibao/common.fun.php';
if (empty($_GET['page']) || is_numeric($_GET['page']) === false) {
    $page = 1;
} else {
    $page = intval($_GET['page']);
    $page = max(1, $page);
}
$server_url='action=plugins&operation=config&do=' . $pluginid . '&identifier=csdn123com_kuaibao&pmod=success';
if($_GET['formhash'] == FORMHASH && empty($_GET['del'])==false && is_numeric($_GET['del'])==true){
	
	$delid=intval($_GET['del']);
	DB::delete('csdn123kuaibao_news','ID=' . $delid);
	cpmsg('csdn123com_kuaibao:del_send',$server_url . '&page=' . $_GET['page'], 'succeed');

} elseif($_GET['formhash'] == FORMHASH && $_GET['selall'] == 'yes') {
	
	if(empty($_GET['idarray']))
	{
		cpmsg('csdn123com_kuaibao:select_empty', $server_url . '&page=' . $_GET['page'], 'succeed');
	}
	$idstr = implode(',', $_GET['idarray']);
	$idstr = daddslashes($idstr);
	DB::delete('csdn123kuaibao_news','ID in (' . $idstr . ')');	
	cpmsg('csdn123com_kuaibao:del_send',$server_url . '&page=' . $_GET['page'], 'succeed');
	
} else {
	
	$startNum = ($page - 1) * 20;
	$postRs=DB::fetch_all("SELECT * FROM " . DB::table("csdn123kuaibao_news") . " where tid>0 ORDER BY tid DESC LIMIT " . $startNum . ",20");
	$nextPage = $server_url . '&page=' . ($page + 1);
    $prePage = $server_url . '&page=' . ($page - 1);
	include template("csdn123com_kuaibao:success");

}