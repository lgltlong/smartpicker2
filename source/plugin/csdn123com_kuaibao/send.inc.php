<?php
if (!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
    exit('Access Denied');
}
require './source/plugin/csdn123com_kuaibao/common.fun.php';
if (empty($_GET['page']) || is_numeric($_GET['page']) === false) {
    $page = 1;
} else {
    $page = intval($_GET['page']);
    $page = max(1, $page);
}
$server_url = 'action=plugins&operation=config&do=' . $pluginid . '&identifier=csdn123com_kuaibao&pmod=send';
if ($_GET['formhash'] == FORMHASH && $_GET['getrecordform'] == 'yes') {
	
    $fid = $_GET['fid'];
    $threadtypeid = $_GET['threadtypeid'];
    $uidstr = $_GET['uidstr'];
	$rndDirec = time();
	$rndDirec = $rndDirec % 3;
	if($rndDirec==2)
	{
		
		$kuaibaoRndUrl=kuaiboRndNews();
		$kuaibaoRndHtmlcode=dfsockopen($kuaibaoRndUrl);
		if(strlen($kuaibaoRndHtmlcode)<200)
		{
			$kuaibaoRndHtmlcode = dfsockopen($kuaibaoRndUrl,0,'','',FALSE,'',15,TRUE,'URLENCODE',FALSE);
		}
		$kuaibaoRndHtmlcode=base64_encode($kuaibaoRndHtmlcode);
		$thread = dfsockopen("http://discuz.csdn123.net/catch/kuaibao/kuaibao_rnd_news.php",0,array('kuaibaoRndHtmlcode'=>$kuaibaoRndHtmlcode));

	} else {
		
		$thread = dfsockopen("http://discuz.csdn123.net/catch/kuaibao/new.php");
		
	}	
    $thread = unserialize($thread);
    foreach ($thread as $threadValue) {
        $chk = DB::fetch_first("SELECT ID FROM " . DB::table('csdn123kuaibao_news') . " WHERE fromurl='" . daddslashes($threadValue['fromurl']) . "'  LIMIT 1");
        if (empty($chk)) {
			$subject = diconv($threadValue['subject'], 'UTF-8');
            $threadValue['subject'] = daddslashes($subject);
            $threadValue['fid'] = daddslashes($fid);
            $threadValue['typeid'] = daddslashes($threadtypeid);
            $threadValue['uidstr'] = daddslashes($uidstr);
            DB::insert('csdn123kuaibao_news', $threadValue);
        }
    }
    cpmsg('csdn123com_kuaibao:rnd_tieba_num', $server_url, 'succeed');
	
} elseif ($_GET['formhash'] == FORMHASH && $_GET['selall'] == 'yes') {
   
   if(empty($_GET['idarray']))
	{
		cpmsg('csdn123com_kuaibao:select_empty', $server_url . '&page=' . $_GET['page'], 'succeed');
	}
    if (empty($_GET['seldelete']) == false) {		
		
		$idstr = implode(',', $_GET['idarray']);
		$idstr = daddslashes($idstr);
        DB::update('csdn123kuaibao_news', array('del' => 1), 'ID in (' . $idstr . ')');
        cpmsg('csdn123com_kuaibao:del_success', $server_url . '&page=' . $_GET['page'], 'succeed');
		
    }
	if (empty($_GET['selimport']) == false) {
	
		if(empty($_GET['num']))
		{
			$num=0;
		} else {
			$num=$_GET['num'];
			$num=intval($num);
		}
		if(is_array($_GET['idarray']))
		{			
			$idarray = $_GET['idarray'];
			$count_num = count($idarray);			
			$idstr = implode(',', $_GET['idarray']);
			
		} else {
			
			 $idstr = $_GET['idarray'];
			 $idarray = explode(',',$idstr);
			 $count_num = count($idarray);			 
		}		
		$ID=$idarray[$num];
		if(is_numeric($ID)==false)
		{
			$success_Url = 'action=plugins&operation=config&do=' . $pluginid . '&identifier=csdn123com_kuaibao&pmod=success';
        	cpmsg('csdn123com_kuaibao:catch_ok', $success_Url, 'succeed');
		}
		$recode = send_thread($ID);
   		if ($recode == 'ok') {
		
			$num++;
			$ID=$idarray[$num];
			if(is_numeric($ID)==false)
			{
				$success_Url = 'action=plugins&operation=config&do=' . $pluginid . '&identifier=csdn123com_kuaibao&pmod=success';
				cpmsg('csdn123com_kuaibao:catch_ok', $success_Url, 'succeed');
			}
			$nextCatchUrl='?' . $server_url . '&selimport=yes&selall=yes&formhash=' . FORMHASH . '&idarray=' . $idstr . '&num=' . $num;
			$statusStr=lang('plugin/csdn123com_kuaibao', 'all_import');
			$statusStr=str_replace('count',$count_num,$statusStr);
			$statusStr=str_replace('num',$num,$statusStr);
			echo '<div style="font-size:20px;margin:8px auto;text-align:center;color:red">' . $statusStr . '</div>';
			echo '<script>setTimeout(function(){ window.location.href="' . $nextCatchUrl . '" },8000);</script>';
		
		} else {
			
			$num++;
			$ID=$idarray[$num];
			if(is_numeric($ID)==false)
			{
				$success_Url = 'action=plugins&operation=config&do=' . $pluginid . '&identifier=csdn123com_kuaibao&pmod=success';
				cpmsg('csdn123com_kuaibao:catch_ok', $success_Url, 'succeed');
			}
			$nextCatchUrl='?' . $server_url . '&selimport=yes&selall=yes&formhash=' . FORMHASH . '&idarray=' . $idstr . '&num=' . $num;
			$statusStr=lang('plugin/csdn123com_kuaibao', 'all_import_err');
			$statusStr=str_replace('num',$num,$statusStr);
			echo '<div style="font-size:20px;margin:8px auto;text-align:center;color:green">' . dhtmlspecialchars($statusStr) . '</div>';
			echo '<script>setTimeout(function(){ window.location.href="' . $nextCatchUrl . '" },5000);</script>';
			
		}		
	
	}	
	
} elseif ($_GET['formhash'] == FORMHASH && $_GET['getrecord'] == 'yes') {
	
    require_once libfile('function/forumlist');
    include template("csdn123com_kuaibao:send_getrecord");
	
} elseif ($_GET['formhash'] == FORMHASH && empty($_GET['del']) == false) {
	
    $delid = intval($_GET['del']);
    DB::update('csdn123kuaibao_news', array('del' => 1), 'ID=' . $delid);
    $del_backUrl = $server_url . '&page=' . $page;
    cpmsg('csdn123com_kuaibao:del_success', $del_backUrl, 'succeed');
	
} elseif ($_GET['formhash'] == FORMHASH && empty($_GET['import_id']) == false && is_numeric($_GET['import_id'])) {
	
    $recode = send_thread($_GET['import_id']);
    if ($recode == 'ok') {
        $success_Url = 'action=plugins&operation=config&do=' . $pluginid . '&identifier=csdn123com_kuaibao&pmod=success';
        cpmsg('csdn123com_kuaibao:catch_ok', $success_Url, 'succeed');
    } else {
        cpmsg('csdn123com_kuaibao:catch_err', '', 'error');
    }
	
} elseif ($_GET['formhash'] == FORMHASH && empty($_GET['update_id']) == false && is_numeric($_GET['update_id'])) {
	
    $update_id = intval($_GET['update_id']);
    $postRs = DB::fetch_first("SELECT * FROM " . DB::table("csdn123kuaibao_news") . " WHERE ID=" . $update_id);
    $typeclassArr = C::t('forum_threadclass')->fetch_all_by_fid($postRs['fid']);
    require_once libfile('function/forumlist');
    include template("csdn123com_kuaibao:send_update");
	
} elseif ($_GET['formhash'] == FORMHASH && empty($_GET['update']) == false && $_GET['update'] == 'yes') {
	
    $newsArr = array();
    $newsArr['subject'] = daddslashes($_GET['subject']);
    $newsArr['fid'] = daddslashes($_GET['fid']);
    $newsArr['typeid'] = daddslashes($_GET['threadtypeid']);
    $newsArr['uidstr'] = daddslashes($_GET['uidstr']);
    DB::update('csdn123kuaibao_news', $newsArr, 'ID=' . intval($_GET['news_id']));
    $update_backUrl = $server_url . '&page=' . $page;
    cpmsg('csdn123com_kuaibao:modify_success', $update_backUrl, 'succeed');
	
} else {
	
    $startNum = ($page - 1) * 20;
    $postRs = DB::fetch_all("SELECT * FROM " . DB::table("csdn123kuaibao_news") . " WHERE tid=0 and del=0 ORDER BY ID DESC LIMIT " . $startNum . ",20");
    $nextPage = $server_url . '&page=' . ($page + 1);
    $prePage = $server_url . '&page=' . ($page - 1);
    include template("csdn123com_kuaibao:send");
}
