<?php
if (!defined('IN_DISCUZ') || !defined('IN_ADMINCP')) {
	exit('Access Denied');
}

function csdn123_avatar_by_uid($uid, $img) {
	global $_G;
	C::t('common_member')->update($uid, array('avatarstatus' => 1));
	$uc_server = basename($_G['setting']['ucenterurl']);
	$dir = DISCUZ_ROOT . './' . $uc_server . '/data/avatar';
	$uid = sprintf("%09d", $uid);
	$dir1 = substr($uid, 0, 3);
	$dir2 = substr($uid, 3, 2);
	$dir3 = substr($uid, 5, 2);
	!is_dir($dir . '/' . $dir1) && mkdir($dir . '/' . $dir1, 0777);
	!is_dir($dir . '/' . $dir1 . '/' . $dir2) && mkdir($dir . '/' . $dir1 . '/' . $dir2, 0777);
	!is_dir($dir . '/' . $dir1 . '/' . $dir2 . '/' . $dir3) && mkdir($dir . '/' . $dir1 . '/' . $dir2 . '/' . $dir3, 0777);
	$avatar_small = $dir . '/' . $dir1 . '/' . $dir2 . '/' . $dir3 . '/' . substr($uid, -2) . "_avatar_small.jpg";
	$avatar_middle = $dir . '/' . $dir1 . '/' . $dir2 . '/' . $dir3 . '/' . substr($uid, -2) . "_avatar_middle.jpg";
	$avatar_big = $dir . '/' . $dir1 . '/' . $dir2 . '/' . $dir3 . '/' . substr($uid, -2) . "_avatar_big.jpg";
	$imgData = dfsockopen($img);
	file_put_contents($avatar_small, $imgData);
	file_put_contents($avatar_middle, $imgData);
	file_put_contents($avatar_big, $imgData);
	
}
if ($_GET['formhash'] == FORMHASH && empty($_GET['adduser']) == false && $_GET['adduser'] == 'yes') {
		
	$regData = dfsockopen('http://www.csdn123.net/plugin/usereg/get_user.php?siteurl=' . urlencode($_G['siteurl']) . '&encode=' . CHARSET);
	$regData = dunserialize($regData);
	$newgroupid=$_GET['newgroupid'];
	$newgroupid=intval($newgroupid);
	foreach ($regData as $regValue) {
		$regValue = diconv($regValue,'UTF-8');
		$regValue = explode('","', $regValue);
		$csdn123_username = preg_replace('/^"/', '', $regValue[0]);
		$csdn123_img = preg_replace('/"$/', '', $regValue[1]);
		if ($_GET['pws'] == 'xxxxxxxxxx') {
			$csdn123_password = random(10);
		} else {
			$csdn123_password = daddslashes($_GET['pws']);
		}
		$csdn123_email = uniqid() . '@qq.com';
		if (C::t('common_member')->fetch_uid_by_username($csdn123_username) || C::t('common_member_archive')->fetch_uid_by_username($csdn123_username)) {
			continue;
		}
		loaducenter();
		$uid = uc_user_register(addslashes($csdn123_username), $csdn123_password, $csdn123_email);
		if ($uid <= 0) {
			continue;
		}
		loadcache('fields_register');
		$init_arr = explode(',', $_G['setting']['initcredits']);
		$common_member_password = md5(random(10));
		C::t('common_member')->insert($uid, $csdn123_username, $common_member_password, $csdn123_email, 'Manual Acting', $newgroupid, $init_arr, 0);
		$avatar = csdn123_avatar_by_uid($uid, $csdn123_img);
		$addUser = array();
		$addUser['uid'] = $uid;
		$addUser['username'] = $csdn123_username;
		$addUser['username_pwd'] = $csdn123_password;
		DB::insert('csdn123kuaibao_reguser', $addUser);
	}
	$user_list = DB::fetch_all('select * from ' . DB::table('csdn123kuaibao_reguser') . ' order by uid desc');
	$server_url='action=plugins&operation=config&do=' . $pluginid . '&identifier=csdn123com_kuaibao&pmod=admin_user';
	$totalmembers = C::t('common_member')->count();	
	$data = array('totalmembers' => $totalmembers, 'newsetuser' => $csdn123_username);
	savecache('userstats', $data);
	cpmsg("csdn123com_kuaibao:reguser_num_ok",$server_url,"succeed");
	
} else {

	$groupselect = array();
		$query = C::t('common_usergroup')->fetch_all_by_not_groupid(array(5, 6, 7));
		foreach($query as $group) {
			$group['type'] = $group['type'] == 'special' && $group['radminid'] ? 'specialadmin' : $group['type'];
			if($group['type'] == 'member' && $group['creditshigher'] == 0) {
				$groupselect[$group['type']] .= "<option value=\"$group[groupid]\" selected>$group[grouptitle]</option>\n";
			} else {
				$groupselect[$group['type']] .= "<option value=\"$group[groupid]\">$group[grouptitle]</option>\n";
			}
		}
		$groupselect = '<optgroup label="'.$lang['usergroups_member'].'">'.$groupselect['member'].'</optgroup>'.
			($groupselect['special'] ? '<optgroup label="'.$lang['usergroups_special'].'">'.$groupselect['special'].'</optgroup>' : '').
			($groupselect['specialadmin'] ? '<optgroup label="'.$lang['usergroups_specialadmin'].'">'.$groupselect['specialadmin'].'</optgroup>' : '').
			'<optgroup label="'.$lang['usergroups_system'].'">'.$groupselect['system'].'</optgroup>';
	$server_url='action=plugins&operation=config&do=' . $pluginid . '&identifier=csdn123com_kuaibao&pmod=csdn123_reguser';		
	include template('csdn123com_kuaibao:reg_user');
}
