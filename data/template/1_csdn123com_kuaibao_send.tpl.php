<?php if(!defined('IN_DISCUZ')) exit('Access Denied'); ?>
<form name="cpform" method="post" autocomplete="off" action="?<?php echo $server_url;?>&selall=yes" id="cpform">
  <input type="hidden" name="formhash" id="formhash" value="<?php echo FORMHASH;?>" />
  <input type="hidden" name="page" value="<?php echo $page;?>" />
  <table class="tb tb2 nobdb">
    <tbody>
      <tr class="header">
        <th>&nbsp;&nbsp;</th>
        <th>天天快报的原始帖子</th>
        <th>论坛版块</th>
        <th>主题分类</th>
        <th>采集回复数</th>
        <th>操作</th>
      </tr>
      <tr>
        <td>&nbsp;</td>
        <td><a class="addtr" href="?<?php echo $server_url;?>&getrecord=yes&formhash=<?php echo FORMHASH;?>">没有内容怎么办？随机采集一批天天快报的内容！</a></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      <?php if(is_array($postRs)) foreach($postRs as $post) { ?>      
      <tr class="hover">
        <td><input type="checkbox" name="idarray[]" value="<?php echo $post['ID'];?>" /></td>
        <td><a href="<?php echo dhtmlspecialchars($post['fromurl'])?>" target="_blank"><?php echo dhtmlspecialchars($post['subject'])?></a></td>
        <td><?php echo getFidName($post['fid'])?></td>
        <td><?php echo getTypeidName($post['typeid'],$post['fid'])?></td>        
        <td id="myid<?php echo $post['ID'];?>">
        
        	<a href="?<?php echo $server_url;?>&update_id=<?php echo $post['ID'];?>&page=<?php echo $page;?>&formhash=<?php echo FORMHASH;?>">修改</a>&nbsp;|&nbsp; 
            <a href="?<?php echo $server_url;?>&import_id=<?php echo $post['ID'];?>&page=<?php echo $page;?>&formhash=<?php echo FORMHASH;?>" onClick="importtobbs('myid<?php echo $post['ID'];?>')">导入</a>&nbsp;|&nbsp; 
            <a href="?<?php echo $server_url;?>&del=<?php echo $post['ID'];?>&page=<?php echo $page;?>&formhash=<?php echo FORMHASH;?>">删除</a>
            
         </td>
      </tr>
      
      <?php } ?>
      
      <tr>
        <td colspan="7"><div style="margin:8px 0;">
            <input name="chkall" id="chkall" type="checkbox" class="checkbox" onclick="checkAll('prefix', this.form, 'idarray', 'chkall')" />
            <label for="chkall">全选</label>
            &nbsp;&nbsp;
            <input type="submit" value="全部导入" name="selimport" class="btn" id="selimport" onClick="this.value='导入中……'" />
            &nbsp;&nbsp;&nbsp;&nbsp;
            <input type="submit" value="全部删除" name="seldelete" class="btn" />
          </div></td>
      </tr>
    </tbody>
  </table>
  <div class="pg" style="margin-top:16px;"> <a href="?<?php echo $prePage;?>&formhash=<?php echo FORMHASH;?>">上一页</a> &nbsp;&nbsp;<?php echo $page;?>&nbsp;&nbsp; <a href="?<?php echo $nextPage;?>&formhash=<?php echo FORMHASH;?>">下一页</a> </div>
</form>
<script>
function importtobbs(myid)
{
document.getElementById(myid).innerHTML='<span style="color:red">导入中……</span>';
}
</script>