<?php if(!defined('IN_DISCUZ')) exit('Access Denied'); ?>
<div class="container" id="cpcontainer">

  <form name="cpform" method="post" action="?<?php echo dhtmlspecialchars($_SERVER['QUERY_STRING'] . '&adduser=yes')?>" id="cpform">

    <input type="hidden" name="formhash" id="formhash" value="<?php echo FORMHASH;?>" />

    <table class="tb tb2 " id="tips">

      <tbody>

        <tr>

          <th class="partition" colspan="2">提示</th>

        </tr>

        <tr>

          <td class="tipsblock" s="1" colspan="2"><ul id="tipslis">

              <li>用户昵称和头像都是真实采集的数据。</li>

              <li>初始密码默认由系统随机生成。</li>

              <li>如果想统一初始密码，请直接输入您想设置的初始密码。</li>

              <li>一次随机批量注册10个用户，如果想注册更多的用户，请重复操作。</li>

              <?php if(strpos($_G['setting']['ucenterurl'], $_G['siteurl']) === false ) { ?>

              <li><span style="color:#F00">ucenter和论坛可能不在同一个站点下面，会导致用户头像无法显示，您可以点击头像重新设置。</span></li>

              <?php } ?>

            </ul></td>

        </tr>

        <tr>

          <td colspan="2" class="td27" s="1"> 初始密码</td>

        </tr>

        <tr class="noborder" onmouseover="setfaq(this, 'faqd3e6')">

          <td class="vtop rowform"><input type="text" value="xxxxxxxxxx" name="pws" class="txt"  /></td>

          <td class="vtop tips2" s="1">&nbsp;</td>

        </tr>

        <tr>

          <td colspan="2" class="td27" s="1"> 用户组</td>

        </tr>

        <tr class="noborder" onmouseover="setfaq(this, 'faqd3e6')">

          <td class="vtop rowform"><select name="newgroupid">

              

              <?php echo $groupselect;?>

            

            </select></td>

          <td class="vtop tips2" s="1">&nbsp;</td>

        </tr>

        <tr>

          <td colspan="2" class="td27" s="1"><div style="margin-top:8px;">

              <input type="submit" class="btn" value="确定注册">

            </div></td>

        </tr>

      </tbody>

    </table>

  </form>

</div>

