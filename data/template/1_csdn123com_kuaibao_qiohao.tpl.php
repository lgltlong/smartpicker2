<?php if(!defined('IN_DISCUZ')) exit('Access Denied'); ?>
<div class="container" id="cpcontainer">

  <form name="cpform" method="post" action="?<?php echo $server_url;?>&search=yes'" id="cpform">
    <input type="hidden" name="formhash" id="formhash" value="<?php echo FORMHASH;?>" />
    <table class="tb tb2 ">
      <tbody>
        <tr>
          <td colspan="2" class="td27" s="1">请输入企鹅号的地址</td>
        </tr>
        <tr class="noborder" onmouseover="setfaq(this, 'faqd3e6')">
          <td class="vtop rowform"><input name="keyword"  type="text" class="txt" ></td>
          <td class="vtop tips2" s="1"><strong>提示：</strong>地址类似 http://kuaibao.qq.com/s/MEDIANEWSLIST?chlid=5407440 </td>
        </tr>
        <tr>
          <td colspan="2" class="td27" s="1">论坛版块:</td>
        </tr>
        <tr class="noborder" onmouseover="setfaq(this, 'faqd3e6')">
          <td class="vtop rowform"><select name="fid" id="fid" onChange="ajaxget('forum.php?mod=ajax&action=getthreadtypes&fid=' + this.value, 'threadtypes');">
              
              <?php echo forumselect(FALSE, 0, 0, TRUE)?>              
            </select></td>
          <td class="vtop tips2" s="1"><strong>提示：</strong>请选择一个论坛版块，采集的结果将发布到这个版块。</td>
        </tr>
        <tr>
          <td colspan="2" class="td27" s="1">主题分类：</td>
        </tr>
        <tr class="noborder" onmouseover="setfaq(this, 'faqd3e6')">
          <td class="vtop rowform"><span id="threadtypes">
            <select name="threadtypeid">
              <option value="0"></option>
            </select>
            </span></td>
          <td class="vtop tips2" s="1"><strong>提示：</strong>请选择论坛版块的主题分类。</td>
        </tr>
        <tr>
          <td colspan="2" class="td27" s="1">请输入发帖用户的UID:</td>
        </tr>
        <tr class="noborder" onmouseover="setfaq(this, 'faqd3e6')">
          <td class="vtop rowform"><input name="uidstr"  type="text" class="txt"  value="<?php echo getRndUid();?>"></td>
          <td class="vtop tips2" s="1"><strong>提示：</strong>UID用英文逗号分隔，如果没有马甲数据，请点击顶部【注册马甲】生成</td>
        </tr>
        <tr>
          <td colspan="2"><div class="fixsel"><br/>
              <br/>
              <input type="submit" class="btn" id="submit_editsubmit" name="editsubmit" value="采集该企鹅号的内容" onClick="this.value='采集中，稍等……'">
              <br/>
              <br/>
              <br/>
            </div></td>
        </tr>
      </tbody>
    </table>
  </form>
  
</div>

<script defer>
var hze_fid=document.getElementById('fid').value;
ajaxget('forum.php?mod=ajax&action=getthreadtypes&fid=' + hze_fid, 'threadtypes');
</script>