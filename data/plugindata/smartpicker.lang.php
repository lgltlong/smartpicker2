﻿<?php

/*
* DIY插件Smartpicker语言包
*/

$scriptlang['smartpicker'] = array(
  'english' => 'chinese',
  
  
  
);

$templatelang['smartpicker'] = array(
	
	/*通用类型的语言包*/
	'more'     => '备注',
	'must'     => '必填',
	'submit'   => '提交',
	'optional' => '选填',
  
  /*后台关键词部分*/
  'kw_welcome'    => '输入网站需要的关键词',
  'kw_name'       => '名称',
  'kw_short'      => '英文缩写',
  'kw_level'      => '关键词层级',
  'kw_father'     => '父关键词',
  'kw_notice_one' => 'NOTICE:关键词名称、关键词缩写、关键词层级以及父关键词为必填必选，备注为选填',
  
  'kw_xxllx' => '新西兰留学',
  'kw_xxllxfy' => '新西兰留学费用',
  'kw_xxllxtj' => '新西兰留学条件',
  'kw_xxllxys' => '新西兰留学优势',
  'kw_xxllxjy' => '新西兰留学经验',
  'kw_xxllxw' => '新西兰留学网',
  'kw_xxldx' => '新西兰大学',
  
  /*后台外部链接部分*/
  'ol_welcome' => '请输入外部链接',
  'ol_title'   => '标题',
  'ol_link'    => '链接',
  'ol_notice'  => '尽量避免重复提交，会造成数据库中产生很多空数据',
  'ol_keyword' => '关键词',
  
  /*后台知识段输出部分*/
  'ack_welcome' => '请输入新的知识段',
  'ack_keyword' => '对应关键词',
  'ack_content' => '请输入内容',
  
  
  
);

$installlang['smartpicker'] = array(
  'english' => 'chinese',
  
);





?>